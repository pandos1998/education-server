const Router = require('express');
const router = new Router();
const controller = require('./authController');
const { check } = require('express-validator');
const authMiddleware = require('./middleware/authMiddleware');

router.post(
    '/registration',
    [
        check('username', 'User input cannot be empty').notEmpty(),
        check(
            'password',
            'Password should be more than 6 and less tham 10 symbols',
        ).isLength({ min: 6, max: 10 }),
    ],
    controller.registration,
);
router.post('/login', controller.login);
router.get('/users', authMiddleware, controller.getUsers);

module.exports = router;
