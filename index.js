const express = require('express');
const mongoose = require('mongoose');
const PORT = process.env.PORT || 5000;
const DB_CONNECT_URL =
    'mongodb+srv://qwerty:qwerty123@cluster0.ytdlrga.mongodb.net/?retryWrites=true&w=majority';
const authRouter = require('./authRouter');

const app = express();

app.use(express.json());
app.use('/auth', authRouter);

(async () => {
    try {
        await mongoose.connect(DB_CONNECT_URL);
        app.listen(PORT, () => console.log('Start on ', PORT));
    } catch (e) {
        console.log({ e });
    }
})();
