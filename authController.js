const bcrypt = require('bcryptjs');
const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');
const { secret } = require('./config');

const Role = require('./models/Role');
const User = require('./models/User');

const generateAccessToken = ({ id, role }) => {
    const payload = {
        id,
        role,
    };

    return jwt.sign(payload, secret, { expiresIn: '24h' });
};

class AuthController {
    registration = async (req, res) => {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res
                    .status(400)
                    .json({ message: 'Reistration error', errors });
            }
            const { username, password } = req.body;
            const candidate = await User.findOne({ username });
            if (candidate) {
                return res.status(400).json({ message: 'User already exist' });
            }
            const hashPassword = bcrypt.hashSync(password, 7);
            const userRole = await Role.findOne({ value: 'USER' });
            const user = new User({
                username,
                password: hashPassword,
                roles: [userRole.value],
            });
            await user.save();
            return res.json({ message: 'Registration Done' });
        } catch (e) {
            console.log({ e });
            res.status(400).json({ message: 'Registration error' });
        }
    };

    login = async (req, res) => {
        try {
            const { username, password } = req.body;
            const user = await User.findOne({ username });
            if (!user) {
                return res
                    .error(400)
                    .json({ message: `User ${username} not found` });
            }
            const validPassword = bcrypt.compareSync(password, user.password);
            if (!validPassword) {
                return res
                    .error(400)
                    .json({ message: `Password is not correct` });
            }
            const token = generateAccessToken({
                user: user._id,
                role: user.roles,
            });
            return res.json({ token });
        } catch (e) {
            console.log({ e });
            res.status(400).json({ message: 'Login error' });
        }
    };

    getUsers = async (req, res) => {
        try {
            const users = await User.find();
            res.json({ users });
        } catch (e) {
            console.log({ e });
            res.status(400).json({ message: 'GetUser error' });
        }
    };
}

module.exports = new AuthController();
